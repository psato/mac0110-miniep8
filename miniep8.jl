function troca(v, i, j)
  aux = v[i]
  v[i] = v[j]
  v[j] = aux
end

function insercao(v)
  tam = length(v)
  for i in 2:tam
    j = i
    while j > 1
      if compareByValueAndSuit(v[j], v[j-1])
        troca(v, j, j - 1)
      else
        break
      end
      j = j - 1
    end
  end
  return v
end

function compareByValue(x, y)
  numberOrderArray = [ '2', '3', '4', '5', '6', '7', '8', '9', '1', 'J', 'Q', 'K', 'A' ]
  return findfirst(e->e==x[1], numberOrderArray) < findfirst(e->e==y[1], numberOrderArray)
end

function compareByValueAndSuit(x, y)
  suitOrderArray = [ '♦', '♠', '♥', '♣' ]
  x[end] == y[end] && return compareByValue(x ,y)
  return findfirst(e->e==x[end], suitOrderArray) < findfirst(e->e==y[end], suitOrderArray)
end
